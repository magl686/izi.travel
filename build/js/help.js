
(function ($) {
  'use strict';

  var querySelector    = document.querySelector.bind(document),
      querySelectorAll = document.querySelectorAll.bind(document),
      body             = document.body;

  var $pageHeader = $(".page-header"),
      $fakeHeader = $(".fake-header"),
      $pageFooter = $(".page-footer");


  $( document ).ready(function(){

    // Set fake header height
    $fakeHeader.height( $pageHeader.height() );

    // Define sidebar max height
    initSidebarHeight();

    // Define headline height
    initHeadlineHeight();

    // Init scroll for sidebar
    $(".nano").nanoScroller();

    // Init photoslider
    $('.main-slider').bxSlider();

    //Init recommended articles slider
    $('.related-articles--slider').bxSlider({
      pager: false
    });
  });

  $( window ).resize(function() {
    initSidebarHeight();
    initHeadlineHeight();
  });


  // Init headline height
  //--------------------------------------------------
  function initHeadlineHeight(){
    var $headline        = $(".headline"),
        $headlineContent = $(".headline__content");

    var headlineHeight = $( window ).height() - $pageHeader.height() - $pageFooter.height() - 36;
    if ( headlineHeight < 400 ) headlineHeight = 400;

    $headline.height( headlineHeight );
    $headlineContent.height( headlineHeight );
  }

  // Init sidebar max height
  //--------------------------------------------------
  function initSidebarHeight(){
    var $sidebar = $(".sidebar-container"),
        $content = $(".sidebar-content"),
        $article = $(".article-container");

    var sidebarHeight = $( window ).height() - $pageHeader.height() - $pageFooter.height() - 72;
    var contentHeight = $content.height();

    sidebarHeight > contentHeight + 30 ? $sidebar.height( contentHeight + 30 ) : $sidebar.height( sidebarHeight );

    $article.css('min-height', sidebarHeight + 34);
  }

  // Sidebar collapse/expand subitems
  //--------------------------------------------------
  $(".sidebar-menu").on("click", ".sidebar-pointer", function(ev) {
    $(this).closest('li').toggleClass("js-sidebar-open-subitems");

    // Recalculate sidebar height
    initSidebarHeight();

    // Recalculate scroll for sidebar
    $(".nano").nanoScroller();

    ev.stopPropagation();
    ev.preventDefault();
  });

  // Prevent scroll bubbling
  //--------------------------------------------------
  $(".nano-content").on("DOMMouseScroll mousewheel", function(ev) {
    var $this        = $(this),
        scrollTop    = this.scrollTop,
        scrollHeight = this.scrollHeight,
        height       = $this.height(),
        delta        = ev.originalEvent.wheelDelta,
        up           = delta > 0;

    var prevent = function() {
      ev.stopPropagation();
      ev.preventDefault();
      ev.returnValue = false;
      return false;
    };

    if (!up && scrollTop - delta > scrollHeight - height) {
      $this.scrollTop(scrollHeight);
      return prevent();
    } else if (up && scrollTop - delta < 0 ) {
      $this.scrollTop(0);
      return prevent();
    }
  });


  // Show / hide help section content
  //--------------------------------------------------
  $(".help-section").on("click",function(){
    var $this = $(this);

    $this.toggleClass('help-section--open');
    $this.next().toggleClass("help-content--open");

    $('html, body').animate({scrollTop: $this.offset().top - 100 }, 800);
  });


  // Show / hide help content sub items
  //--------------------------------------------------
  $(".help-content__item").on("click",function(){
    var $this = $(this);

    $this.toggleClass('help-content__item--open');
  });

  $(".help-content__item a").on("click",function(e){
    e.stopPropagation();
  });


  // Language selector
  //--------------------------------------------------
  
  
  // Language selector
  //--------------------------------------------------
  var langSelector = querySelector('.lang-selector');
  
  function closeLangSelector() {
    langSelector.classList.remove('js-show-lang-selector');
  }
  function toggleLangSelector(ev) {
    langSelector.classList.toggle('js-show-lang-selector');
    ev.stopPropagation();
  }
  
  langSelector.addEventListener('click', function(event) {
    toggleLangSelector(event);
  });
  
  body.addEventListener('click', function() {
    closeLangSelector();
  });


  // Hide / show search form for menu
  //--------------------------------------------------
  $(".search-item__open-bt").on("click", function(ev) {
    var $searchItem = $(this).closest(".search-item");

    $searchItem.toggleClass("js-hide-search-input");
    $searchItem.find(".search-item__input").focus();

    ev.stopPropagation();
  });
  $(".search-item").on("click", function(ev) {
    ev.stopPropagation();
  });
  function hideSearchInput(){
    var $searchItem = $(".wp-main-menu .search-item");
    $searchItem.addClass("js-hide-search-input");
  }


  body.addEventListener('click', function() {
    hideSearchInput();
  });
})(jQuery);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJoZWxwLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxyXG4oZnVuY3Rpb24gKCQpIHtcclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIHZhciBxdWVyeVNlbGVjdG9yICAgID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvci5iaW5kKGRvY3VtZW50KSxcclxuICAgICAgcXVlcnlTZWxlY3RvckFsbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwuYmluZChkb2N1bWVudCksXHJcbiAgICAgIGJvZHkgICAgICAgICAgICAgPSBkb2N1bWVudC5ib2R5O1xyXG5cclxuICB2YXIgJHBhZ2VIZWFkZXIgPSAkKFwiLnBhZ2UtaGVhZGVyXCIpLFxyXG4gICAgICAkZmFrZUhlYWRlciA9ICQoXCIuZmFrZS1oZWFkZXJcIiksXHJcbiAgICAgICRwYWdlRm9vdGVyID0gJChcIi5wYWdlLWZvb3RlclwiKTtcclxuXHJcblxyXG4gICQoIGRvY3VtZW50ICkucmVhZHkoZnVuY3Rpb24oKXtcclxuXHJcbiAgICAvLyBTZXQgZmFrZSBoZWFkZXIgaGVpZ2h0XHJcbiAgICAkZmFrZUhlYWRlci5oZWlnaHQoICRwYWdlSGVhZGVyLmhlaWdodCgpICk7XHJcblxyXG4gICAgLy8gRGVmaW5lIHNpZGViYXIgbWF4IGhlaWdodFxyXG4gICAgaW5pdFNpZGViYXJIZWlnaHQoKTtcclxuXHJcbiAgICAvLyBEZWZpbmUgaGVhZGxpbmUgaGVpZ2h0XHJcbiAgICBpbml0SGVhZGxpbmVIZWlnaHQoKTtcclxuXHJcbiAgICAvLyBJbml0IHNjcm9sbCBmb3Igc2lkZWJhclxyXG4gICAgJChcIi5uYW5vXCIpLm5hbm9TY3JvbGxlcigpO1xyXG5cclxuICAgIC8vIEluaXQgcGhvdG9zbGlkZXJcclxuICAgICQoJy5tYWluLXNsaWRlcicpLmJ4U2xpZGVyKCk7XHJcblxyXG4gICAgLy9Jbml0IHJlY29tbWVuZGVkIGFydGljbGVzIHNsaWRlclxyXG4gICAgJCgnLnJlbGF0ZWQtYXJ0aWNsZXMtLXNsaWRlcicpLmJ4U2xpZGVyKHtcclxuICAgICAgcGFnZXI6IGZhbHNlXHJcbiAgICB9KTtcclxuICB9KTtcclxuXHJcbiAgJCggd2luZG93ICkucmVzaXplKGZ1bmN0aW9uKCkge1xyXG4gICAgaW5pdFNpZGViYXJIZWlnaHQoKTtcclxuICAgIGluaXRIZWFkbGluZUhlaWdodCgpO1xyXG4gIH0pO1xyXG5cclxuXHJcbiAgLy8gSW5pdCBoZWFkbGluZSBoZWlnaHRcclxuICAvLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgZnVuY3Rpb24gaW5pdEhlYWRsaW5lSGVpZ2h0KCl7XHJcbiAgICB2YXIgJGhlYWRsaW5lICAgICAgICA9ICQoXCIuaGVhZGxpbmVcIiksXHJcbiAgICAgICAgJGhlYWRsaW5lQ29udGVudCA9ICQoXCIuaGVhZGxpbmVfX2NvbnRlbnRcIik7XHJcblxyXG4gICAgdmFyIGhlYWRsaW5lSGVpZ2h0ID0gJCggd2luZG93ICkuaGVpZ2h0KCkgLSAkcGFnZUhlYWRlci5oZWlnaHQoKSAtICRwYWdlRm9vdGVyLmhlaWdodCgpIC0gMzY7XHJcbiAgICBpZiAoIGhlYWRsaW5lSGVpZ2h0IDwgNDAwICkgaGVhZGxpbmVIZWlnaHQgPSA0MDA7XHJcblxyXG4gICAgJGhlYWRsaW5lLmhlaWdodCggaGVhZGxpbmVIZWlnaHQgKTtcclxuICAgICRoZWFkbGluZUNvbnRlbnQuaGVpZ2h0KCBoZWFkbGluZUhlaWdodCApO1xyXG4gIH1cclxuXHJcbiAgLy8gSW5pdCBzaWRlYmFyIG1heCBoZWlnaHRcclxuICAvLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgZnVuY3Rpb24gaW5pdFNpZGViYXJIZWlnaHQoKXtcclxuICAgIHZhciAkc2lkZWJhciA9ICQoXCIuc2lkZWJhci1jb250YWluZXJcIiksXHJcbiAgICAgICAgJGNvbnRlbnQgPSAkKFwiLnNpZGViYXItY29udGVudFwiKSxcclxuICAgICAgICAkYXJ0aWNsZSA9ICQoXCIuYXJ0aWNsZS1jb250YWluZXJcIik7XHJcblxyXG4gICAgdmFyIHNpZGViYXJIZWlnaHQgPSAkKCB3aW5kb3cgKS5oZWlnaHQoKSAtICRwYWdlSGVhZGVyLmhlaWdodCgpIC0gJHBhZ2VGb290ZXIuaGVpZ2h0KCkgLSA3MjtcclxuICAgIHZhciBjb250ZW50SGVpZ2h0ID0gJGNvbnRlbnQuaGVpZ2h0KCk7XHJcblxyXG4gICAgc2lkZWJhckhlaWdodCA+IGNvbnRlbnRIZWlnaHQgKyAzMCA/ICRzaWRlYmFyLmhlaWdodCggY29udGVudEhlaWdodCArIDMwICkgOiAkc2lkZWJhci5oZWlnaHQoIHNpZGViYXJIZWlnaHQgKTtcclxuXHJcbiAgICAkYXJ0aWNsZS5jc3MoJ21pbi1oZWlnaHQnLCBzaWRlYmFySGVpZ2h0ICsgMzQpO1xyXG4gIH1cclxuXHJcbiAgLy8gU2lkZWJhciBjb2xsYXBzZS9leHBhbmQgc3ViaXRlbXNcclxuICAvLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgJChcIi5zaWRlYmFyLW1lbnVcIikub24oXCJjbGlja1wiLCBcIi5zaWRlYmFyLXBvaW50ZXJcIiwgZnVuY3Rpb24oZXYpIHtcclxuICAgICQodGhpcykuY2xvc2VzdCgnbGknKS50b2dnbGVDbGFzcyhcImpzLXNpZGViYXItb3Blbi1zdWJpdGVtc1wiKTtcclxuXHJcbiAgICAvLyBSZWNhbGN1bGF0ZSBzaWRlYmFyIGhlaWdodFxyXG4gICAgaW5pdFNpZGViYXJIZWlnaHQoKTtcclxuXHJcbiAgICAvLyBSZWNhbGN1bGF0ZSBzY3JvbGwgZm9yIHNpZGViYXJcclxuICAgICQoXCIubmFub1wiKS5uYW5vU2Nyb2xsZXIoKTtcclxuXHJcbiAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgfSk7XHJcblxyXG4gIC8vIFByZXZlbnQgc2Nyb2xsIGJ1YmJsaW5nXHJcbiAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICQoXCIubmFuby1jb250ZW50XCIpLm9uKFwiRE9NTW91c2VTY3JvbGwgbW91c2V3aGVlbFwiLCBmdW5jdGlvbihldikge1xyXG4gICAgdmFyICR0aGlzICAgICAgICA9ICQodGhpcyksXHJcbiAgICAgICAgc2Nyb2xsVG9wICAgID0gdGhpcy5zY3JvbGxUb3AsXHJcbiAgICAgICAgc2Nyb2xsSGVpZ2h0ID0gdGhpcy5zY3JvbGxIZWlnaHQsXHJcbiAgICAgICAgaGVpZ2h0ICAgICAgID0gJHRoaXMuaGVpZ2h0KCksXHJcbiAgICAgICAgZGVsdGEgICAgICAgID0gZXYub3JpZ2luYWxFdmVudC53aGVlbERlbHRhLFxyXG4gICAgICAgIHVwICAgICAgICAgICA9IGRlbHRhID4gMDtcclxuXHJcbiAgICB2YXIgcHJldmVudCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgZXYucmV0dXJuVmFsdWUgPSBmYWxzZTtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfTtcclxuXHJcbiAgICBpZiAoIXVwICYmIHNjcm9sbFRvcCAtIGRlbHRhID4gc2Nyb2xsSGVpZ2h0IC0gaGVpZ2h0KSB7XHJcbiAgICAgICR0aGlzLnNjcm9sbFRvcChzY3JvbGxIZWlnaHQpO1xyXG4gICAgICByZXR1cm4gcHJldmVudCgpO1xyXG4gICAgfSBlbHNlIGlmICh1cCAmJiBzY3JvbGxUb3AgLSBkZWx0YSA8IDAgKSB7XHJcbiAgICAgICR0aGlzLnNjcm9sbFRvcCgwKTtcclxuICAgICAgcmV0dXJuIHByZXZlbnQoKTtcclxuICAgIH1cclxuICB9KTtcclxuXHJcblxyXG4gIC8vIFNob3cgLyBoaWRlIGhlbHAgc2VjdGlvbiBjb250ZW50XHJcbiAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICQoXCIuaGVscC1zZWN0aW9uXCIpLm9uKFwiY2xpY2tcIixmdW5jdGlvbigpe1xyXG4gICAgdmFyICR0aGlzID0gJCh0aGlzKTtcclxuXHJcbiAgICAkdGhpcy50b2dnbGVDbGFzcygnaGVscC1zZWN0aW9uLS1vcGVuJyk7XHJcbiAgICAkdGhpcy5uZXh0KCkudG9nZ2xlQ2xhc3MoXCJoZWxwLWNvbnRlbnQtLW9wZW5cIik7XHJcblxyXG4gICAgJCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe3Njcm9sbFRvcDogJHRoaXMub2Zmc2V0KCkudG9wIC0gMTAwIH0sIDgwMCk7XHJcbiAgfSk7XHJcblxyXG5cclxuICAvLyBTaG93IC8gaGlkZSBoZWxwIGNvbnRlbnQgc3ViIGl0ZW1zXHJcbiAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICQoXCIuaGVscC1jb250ZW50X19pdGVtXCIpLm9uKFwiY2xpY2tcIixmdW5jdGlvbigpe1xyXG4gICAgdmFyICR0aGlzID0gJCh0aGlzKTtcclxuXHJcbiAgICAkdGhpcy50b2dnbGVDbGFzcygnaGVscC1jb250ZW50X19pdGVtLS1vcGVuJyk7XHJcbiAgfSk7XHJcblxyXG4gICQoXCIuaGVscC1jb250ZW50X19pdGVtIGFcIikub24oXCJjbGlja1wiLGZ1bmN0aW9uKGUpe1xyXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICB9KTtcclxuXHJcblxyXG4gIC8vIExhbmd1YWdlIHNlbGVjdG9yXHJcbiAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIFxyXG4gIFxyXG4gIC8vIExhbmd1YWdlIHNlbGVjdG9yXHJcbiAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIHZhciBsYW5nU2VsZWN0b3IgPSBxdWVyeVNlbGVjdG9yKCcubGFuZy1zZWxlY3RvcicpO1xyXG4gIFxyXG4gIGZ1bmN0aW9uIGNsb3NlTGFuZ1NlbGVjdG9yKCkge1xyXG4gICAgbGFuZ1NlbGVjdG9yLmNsYXNzTGlzdC5yZW1vdmUoJ2pzLXNob3ctbGFuZy1zZWxlY3RvcicpO1xyXG4gIH1cclxuICBmdW5jdGlvbiB0b2dnbGVMYW5nU2VsZWN0b3IoZXYpIHtcclxuICAgIGxhbmdTZWxlY3Rvci5jbGFzc0xpc3QudG9nZ2xlKCdqcy1zaG93LWxhbmctc2VsZWN0b3InKTtcclxuICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gIH1cclxuICBcclxuICBsYW5nU2VsZWN0b3IuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbihldmVudCkge1xyXG4gICAgdG9nZ2xlTGFuZ1NlbGVjdG9yKGV2ZW50KTtcclxuICB9KTtcclxuICBcclxuICBib2R5LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICBjbG9zZUxhbmdTZWxlY3RvcigpO1xyXG4gIH0pO1xyXG5cclxuXHJcbiAgLy8gSGlkZSAvIHNob3cgc2VhcmNoIGZvcm0gZm9yIG1lbnVcclxuICAvLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgJChcIi5zZWFyY2gtaXRlbV9fb3Blbi1idFwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGV2KSB7XHJcbiAgICB2YXIgJHNlYXJjaEl0ZW0gPSAkKHRoaXMpLmNsb3Nlc3QoXCIuc2VhcmNoLWl0ZW1cIik7XHJcblxyXG4gICAgJHNlYXJjaEl0ZW0udG9nZ2xlQ2xhc3MoXCJqcy1oaWRlLXNlYXJjaC1pbnB1dFwiKTtcclxuICAgICRzZWFyY2hJdGVtLmZpbmQoXCIuc2VhcmNoLWl0ZW1fX2lucHV0XCIpLmZvY3VzKCk7XHJcblxyXG4gICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgfSk7XHJcbiAgJChcIi5zZWFyY2gtaXRlbVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGV2KSB7XHJcbiAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICB9KTtcclxuICBmdW5jdGlvbiBoaWRlU2VhcmNoSW5wdXQoKXtcclxuICAgIHZhciAkc2VhcmNoSXRlbSA9ICQoXCIud3AtbWFpbi1tZW51IC5zZWFyY2gtaXRlbVwiKTtcclxuICAgICRzZWFyY2hJdGVtLmFkZENsYXNzKFwianMtaGlkZS1zZWFyY2gtaW5wdXRcIik7XHJcbiAgfVxyXG5cclxuXHJcbiAgYm9keS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgaGlkZVNlYXJjaElucHV0KCk7XHJcbiAgfSk7XHJcbn0pKGpRdWVyeSk7Il0sImZpbGUiOiJoZWxwLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=