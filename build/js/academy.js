
(function ($) {
  'use strict';

  var querySelector    = document.querySelector.bind(document),
      querySelectorAll = document.querySelectorAll.bind(document),
      body             = document.body;

  var $pageHeader = $(".page-header"),
      $fakeHeader = $(".fake-header"),
      $pageFooter = $(".page-footer");


  $( document ).ready(function(){

    // Set fake header height
    $fakeHeader.height( $pageHeader.height() );

    //Init recommended articles slider
    $('.related-articles--slider').bxSlider({
      pager: false
    });
  });

  $( window ).resize(function() {

  });

  body.addEventListener('click', function() {

  });
})(jQuery);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJhY2FkZW15LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxyXG4oZnVuY3Rpb24gKCQpIHtcclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIHZhciBxdWVyeVNlbGVjdG9yICAgID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvci5iaW5kKGRvY3VtZW50KSxcclxuICAgICAgcXVlcnlTZWxlY3RvckFsbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwuYmluZChkb2N1bWVudCksXHJcbiAgICAgIGJvZHkgICAgICAgICAgICAgPSBkb2N1bWVudC5ib2R5O1xyXG5cclxuICB2YXIgJHBhZ2VIZWFkZXIgPSAkKFwiLnBhZ2UtaGVhZGVyXCIpLFxyXG4gICAgICAkZmFrZUhlYWRlciA9ICQoXCIuZmFrZS1oZWFkZXJcIiksXHJcbiAgICAgICRwYWdlRm9vdGVyID0gJChcIi5wYWdlLWZvb3RlclwiKTtcclxuXHJcblxyXG4gICQoIGRvY3VtZW50ICkucmVhZHkoZnVuY3Rpb24oKXtcclxuXHJcbiAgICAvLyBTZXQgZmFrZSBoZWFkZXIgaGVpZ2h0XHJcbiAgICAkZmFrZUhlYWRlci5oZWlnaHQoICRwYWdlSGVhZGVyLmhlaWdodCgpICk7XHJcblxyXG4gICAgLy9Jbml0IHJlY29tbWVuZGVkIGFydGljbGVzIHNsaWRlclxyXG4gICAgJCgnLnJlbGF0ZWQtYXJ0aWNsZXMtLXNsaWRlcicpLmJ4U2xpZGVyKHtcclxuICAgICAgcGFnZXI6IGZhbHNlXHJcbiAgICB9KTtcclxuICB9KTtcclxuXHJcbiAgJCggd2luZG93ICkucmVzaXplKGZ1bmN0aW9uKCkge1xyXG5cclxuICB9KTtcclxuXHJcbiAgYm9keS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG5cclxuICB9KTtcclxufSkoalF1ZXJ5KTsiXSwiZmlsZSI6ImFjYWRlbXkuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==