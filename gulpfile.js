'use strict';

var gulp        = require('gulp'),
    prefixer    = require('gulp-autoprefixer'),
    uglify      = require('gulp-uglify'),
    less        = require('gulp-less'),
    sourcemaps  = require('gulp-sourcemaps'),
    rigger      = require('gulp-rigger'),
    cssmin      = require('gulp-minify-css'),
    imagemin    = require('gulp-imagemin'),
      pngquant  = require('imagemin-pngquant'),
    cache       = require('gulp-cache'),
    jshint      = require('gulp-jshint'),
    browserSync = require("browser-sync"),
    del         = require('del'),
    gutil       = require('gulp-util'),
    sequence    = require('run-sequence'),
    reload      = browserSync.reload,
    dev         = false,
    prod        = false;


var path = {
  // Куда складывать готовые после сборки файлы
  build: { 
    html:  'build/',
    js:    'build/js/',
    libJS: 'build/lib/',
    style: 'build/css/',
    img:   'build/img/',
    fonts: 'build/fonts/'
  },
  // Пути откуда брать исходники
  src: { 
    html:  'src/*.html', 
    js:    'src/js/*.js',
    libJS: 'src/lib/*.js',
    style: 'src/css/*.*',
    img:   'src/img/**/*.*', 
    fonts: 'src/fonts/**/*.*'
  },
  // За изменением каких файлов мы хотим наблюдать
  watch: { 
    html:  'src/**/*.html',   
    js:    'src/js/**/*.js',
    style: 'src/css/**/*.*',
    img:   'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  }
};


// Конфигурация сервера
// -----------------------------------------------
var config = {
  server: {
    baseDir: "./build"
  },
  // если нужно проверить на живом сайте - name.localtunnel.me
  // tunnel: true ("my-private-site"),  
  host: 'localhost',
  port: 9000,
  logPrefix: "Web machine"
};


// Очиска данных в папках
// -----------------------------------------------
gulp.task('cleanFonts', function (cb) {
  del([path.build.fonts], cb);
});

gulp.task('cleanJS', function (cb) {
  del([path.build.js], cb);
});

gulp.task('cleanStyle', function (cb) {
  del([path.build.style], cb);
});

gulp.task('cleanImage', function (cb) {
  del([path.build.img], cb);
});


// Основные задачи
// -----------------------------------------------
gulp.task('html', function () {
  return gulp.src( path.src.html )
    .pipe( rigger() ) 
    .pipe( gulp.dest(path.build.html) ) 
    .pipe( reload({stream: true}) ); 
});

gulp.task('fonts', ['cleanFonts'], function() {
  return gulp.src( path.src.fonts )
    .pipe( gulp.dest(path.build.fonts) );
});

gulp.task('server', function () {
  browserSync(config);  
});

gulp.task('libJS', function() {
  return gulp.src( path.src.libJS )
    .pipe( gulp.dest(path.build.libJS) );
});


gulp.task('jshint', function() {
  return gulp.src( path.src.js )
    .pipe( jshint() )
    .pipe( jshint.reporter('jshint-stylish') );
});

gulp.task('js', ['cleanJS','jshint'], function () {
  return gulp.src( path.src.js )
    .pipe( rigger() )
    .pipe( !dev ? sourcemaps.init() : gutil.noop() )
    .pipe( prod ? uglify() : gutil.noop() )
    .pipe( !dev ? sourcemaps.write() : gutil.noop() )
    .pipe( gulp.dest(path.build.js) )
    .pipe( reload({stream: true}) );
});

gulp.task('style', ['cleanStyle'], function () {
  return gulp.src( path.src.style )
    .pipe( !dev ? sourcemaps.init() : gutil.noop() )
    .pipe( less() )
    .pipe( prefixer() )
    .pipe( prod ? cssmin() : gutil.noop() )
    .pipe( !dev ? sourcemaps.write() : gutil.noop() )
    .pipe( gulp.dest(path.build.style) )
    .pipe( reload({stream: true}) );
});

gulp.task('image', ['cleanImage'], function () {
  return gulp.src( path.src.img ) 
    .pipe( prod ? cache( imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()],
      interlaced: true
    })) : gutil.noop())
    .pipe( gulp.dest(path.build.img) )
    .pipe( reload({stream: true}) );
});


// Отслеживание изменений
// -----------------------------------------------
gulp.task('watch', function(){
  gulp.watch( path.watch.html, ['html'] );
  gulp.watch( path.watch.style, ['style'] );
  gulp.watch( path.watch.js, ['js'] );
  gulp.watch( path.watch.image, ['image'] );
  gulp.watch( path.watch.fonts, ['fonts'] );
});


// Default сборка
// -----------------------------------------------
gulp.task('build', ['html', 'libJS', 'js', 'style', 'fonts', 'image']);
gulp.task('default', ['build', 'server', 'watch']);

// Production сборка
// -----------------------------------------------
gulp.task('setProduction', function () {
  prod = true; 
});

gulp.task('prod', function () {
  // используем run-sequence module, чтобы 
  // setProduction запустилось раньше остальных задач
  sequence('setProduction', ['build', 'server', 'watch'])
});

// Dev сборка без минификации файлов и без sourcemaps
// -----------------------------------------------
gulp.task('setDevelop', function () {
  dev = true;
});

gulp.task('dev', function () {
  // сборка для тестирования на dev (без минификации)
  sequence('setDevelop', ['build', 'server', 'watch'])
});