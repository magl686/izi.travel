
(function ($) {
  'use strict';

  var querySelector    = document.querySelector.bind(document),
      querySelectorAll = document.querySelectorAll.bind(document),
      body             = document.body;

  var $pageHeader = $(".page-header"),
      $fakeHeader = $(".fake-header"),
      $pageFooter = $(".page-footer");


  $( document ).ready(function(){

    // Set fake header height
    $fakeHeader.height( $pageHeader.height() );

    // Define sidebar max height
    initSidebarHeight();

    // Define headline height
    initHeadlineHeight();

    // Init scroll for sidebar
    $(".nano").nanoScroller();

    // Init photoslider
    $('.main-slider').bxSlider();

    //Init recommended articles slider
    $('.related-articles--slider').bxSlider({
      pager: false
    });
  });

  $( window ).resize(function() {
    initSidebarHeight();
    initHeadlineHeight();
  });


  // Init headline height
  //--------------------------------------------------
  function initHeadlineHeight(){
    var $headline        = $(".headline"),
        $headlineContent = $(".headline__content");

    var headlineHeight = $( window ).height() - $pageHeader.height() - $pageFooter.height() - 36;
    if ( headlineHeight < 400 ) headlineHeight = 400;

    $headline.height( headlineHeight );
    $headlineContent.height( headlineHeight );
  }

  // Init sidebar max height
  //--------------------------------------------------
  function initSidebarHeight(){
    var $sidebar = $(".sidebar-container"),
        $content = $(".sidebar-content"),
        $article = $(".article-container");

    var sidebarHeight = $( window ).height() - $pageHeader.height() - $pageFooter.height() - 72;
    var contentHeight = $content.height();

    sidebarHeight > contentHeight + 30 ? $sidebar.height( contentHeight + 30 ) : $sidebar.height( sidebarHeight );

    $article.css('min-height', sidebarHeight + 34);
  }

  // Sidebar collapse/expand subitems
  //--------------------------------------------------
  $(".sidebar-menu").on("click", ".sidebar-pointer", function(ev) {
    $(this).closest('li').toggleClass("js-sidebar-open-subitems");

    // Recalculate sidebar height
    initSidebarHeight();

    // Recalculate scroll for sidebar
    $(".nano").nanoScroller();

    ev.stopPropagation();
    ev.preventDefault();
  });

  // Prevent scroll bubbling
  //--------------------------------------------------
  $(".nano-content").on("DOMMouseScroll mousewheel", function(ev) {
    var $this        = $(this),
        scrollTop    = this.scrollTop,
        scrollHeight = this.scrollHeight,
        height       = $this.height(),
        delta        = ev.originalEvent.wheelDelta,
        up           = delta > 0;

    var prevent = function() {
      ev.stopPropagation();
      ev.preventDefault();
      ev.returnValue = false;
      return false;
    };

    if (!up && scrollTop - delta > scrollHeight - height) {
      $this.scrollTop(scrollHeight);
      return prevent();
    } else if (up && scrollTop - delta < 0 ) {
      $this.scrollTop(0);
      return prevent();
    }
  });


  // Show / hide help section content
  //--------------------------------------------------
  $(".help-section").on("click",function(){
    var $this = $(this);

    $this.toggleClass('help-section--open');
    $this.next().toggleClass("help-content--open");

    $('html, body').animate({scrollTop: $this.offset().top - 100 }, 800);
  });


  // Show / hide help content sub items
  //--------------------------------------------------
  $(".help-content__item").on("click",function(){
    var $this = $(this);

    $this.toggleClass('help-content__item--open');
  });

  $(".help-content__item a").on("click",function(e){
    e.stopPropagation();
  });


  // Language selector
  //--------------------------------------------------
  //= _components/_language-selector.js


  // Hide / show search form for menu
  //--------------------------------------------------
  $(".search-item__open-bt").on("click", function(ev) {
    var $searchItem = $(this).closest(".search-item");

    $searchItem.toggleClass("js-hide-search-input");
    $searchItem.find(".search-item__input").focus();

    ev.stopPropagation();
  });
  $(".search-item").on("click", function(ev) {
    ev.stopPropagation();
  });
  function hideSearchInput(){
    var $searchItem = $(".wp-main-menu .search-item");
    $searchItem.addClass("js-hide-search-input");
  }


  body.addEventListener('click', function() {
    hideSearchInput();
  });
})(jQuery);