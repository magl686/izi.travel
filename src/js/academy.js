
(function ($) {
  'use strict';

  var querySelector    = document.querySelector.bind(document),
      querySelectorAll = document.querySelectorAll.bind(document),
      body             = document.body;

  var $pageHeader = $(".page-header"),
      $fakeHeader = $(".fake-header"),
      $pageFooter = $(".page-footer");


  $( document ).ready(function(){

    // Set fake header height
    $fakeHeader.height( $pageHeader.height() );

    //Init recommended articles slider
    $('.related-articles--slider').bxSlider({
      pager: false
    });
  });

  $( window ).resize(function() {

  });

  body.addEventListener('click', function() {

  });
})(jQuery);